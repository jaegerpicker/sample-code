#pragma strict
#pragma implicit
#pragma downcast
// Copyright (c) 2010 igamemaker.com

/////////////////////////////////////////////
// ART
/////////////////////////////////////////////

//////////////
// GUISKIN
var customGUISkin : GUISkin;

//////////////
// BACKGROUND
var backGround : Texture2D;

//////////////
// BOARD BUTTONS
var boardButton : Texture2D[];
private var boardButtonWidth : int;
private var boardButtonHeight : int;
boardButtonWidth = boardButton[0].width;
boardButtonHeight = boardButton[0].height;
private var xOffset : int;
private var yOffset : int;
private var xSpacing : int;
private var ySpacing : int;
xOffset = 14;
yOffset = 83;
xSpacing = 6;
ySpacing = 5;




/////////////////////////////////////////////
// GAMEPLAY
/////////////////////////////////////////////
private var turn : int; // -1 = X, 1 = O
turn = -1;
private var boardState: int[]; // State of the baord
// Set board state to off
boardState = new int[9];
for (state in boardState) {
	state = 0;
}

/////////////////////////////////////////////
// MAIN UI RENDERING
/////////////////////////////////////////////
function OnGUI () {
	GUI.skin = customGUISkin;
	
	// Render out the blank background
	GUI.Label(Rect(0,0,320,480), GUIContent(backGround));
	
	// Render the buttons on top of the blank background
	var index : int;
	index = 0;
	for (state in boardState) {			
		var xPos : int;
		var yPos : int;
		xPos = xOffset + index%3 * boardButtonWidth + index%3 * xSpacing;
		yPos = yOffset + index/3 * boardButtonHeight + index/3 * ySpacing;
		
		if (GUI.Button(Rect(xPos,yPos,boardButtonWidth, boardButtonHeight), boardButton[state+1])) {
			state = turn;
			turn = turn * -1;
		}	
		index++;
	}
}

function Update () {
}

// Copyright (c) 2010 igamemaker.com
