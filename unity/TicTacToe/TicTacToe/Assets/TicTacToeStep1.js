#pragma strict
#pragma implicit
#pragma downcast
// Copyright (c) 2010 igamemaker.com

/////////////////////////////////////////////
// ART
/////////////////////////////////////////////

//////////////
// BACKGROUND
var backGround : Texture2D;

/////////////////////////////////////////////
// MAIN UI RENDERING
/////////////////////////////////////////////
function OnGUI () {
	
	// Render out the blank background
	GUI.Label(Rect(0,0,320,480), GUIContent(backGround));
}

function Update () {
}

// Copyright (c) 2010 igamemaker.com
