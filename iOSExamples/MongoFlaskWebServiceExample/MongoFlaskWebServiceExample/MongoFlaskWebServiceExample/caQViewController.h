//
//  caQViewController.h
//  MongoFlaskWebServiceExample
//
//  Created by Shawn Campbell on 7/30/12.
//  Copyright (c) 2012 Shawn Campbell. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface caQViewController : UIViewController {
    IBOutlet UITextView *txtParsed;
    IBOutlet UITextView *jsonTextBox;
    IBOutlet UISegmentedControl *jsonButton;
    IBOutlet UITextField *txtInput;
}
@property (nonatomic, retain) UITextView *txtParsed;
@property (nonatomic, retain) UITextView *jsonTextBox;
@property (nonatomic, retain) UISegmentedControl *jsonButton;
@property (nonatomic, retain) UITextField *txtInput;

-(IBAction) segmentedControlIndexChanged;


@end
