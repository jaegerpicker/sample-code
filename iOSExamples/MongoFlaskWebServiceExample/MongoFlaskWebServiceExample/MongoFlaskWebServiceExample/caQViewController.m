//
//  caQViewController.m
//  MongoFlaskWebServiceExample
//
//  Created by Shawn Campbell on 7/30/12.
//  Copyright (c) 2012 Shawn Campbell. All rights reserved.
//
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) 
#import "caQViewController.h"

@interface caQViewController ()

@end

@implementation caQViewController
@synthesize jsonTextBox;
@synthesize txtParsed;
@synthesize jsonButton;
@synthesize txtInput;



- (void)viewDidLoad
{
    [super viewDidLoad];
    [jsonButton addTarget:self action:@selector(myButtonClick:) forControlEvents:(UIControlEvents)UIControlEventTouchDown];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-  (void)myButtonClick:(id)sender {
    
}

-(IBAction) segmentedControlIndexChanged {
    //[txtInput setText: [NSString stringWithFormat:, txtInput.text]];
    NSLog(@"selected index: %d", self.jsonButton.selectedSegmentIndex);
    NSLog(@"input text: %@", self.txtInput.text);
    switch (self.jsonButton.selectedSegmentIndex) {
        case 0:
        {
            NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"http://127.0.0.1:5000/users.json/%@", txtInput.text]];
            NSLog(@"url: %@", url);
            dispatch_async(kBgQueue, ^{
                NSData* data = [NSData dataWithContentsOfURL: url];
                [self performSelectorOnMainThread:@selector(fetchedData:) withObject:data waitUntilDone:YES];
            });
        }
            break;
        case 1:
        {
            NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"http://127.0.0.1:5000/user.json/%@", txtInput.text]];
            NSLog(@"url: %@", url);
            dispatch_async(kBgQueue, ^{
                NSData* data = [NSData dataWithContentsOfURL: url];
                [self performSelectorOnMainThread:@selector(fetchedData:) withObject:data waitUntilDone:YES];
            });
        }
            break;
        default:
            break;
    }
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

- (void)fetchedData:(NSData *)responseData {
    //parse out the json data
    //NSArray* users = [json objectForKey:@"user"];
    txtParsed.text = @"\n";
    NSError *jsonParsingError = nil;
    NSMutableArray *jsonArray = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers|NSJSONReadingAllowFragments error:&jsonParsingError];
    
    //NSLog(@"jsonList: %@", jsonArray);
    
    if(!jsonArray)
    {
        NSLog(@"Error parsing JSON:%@", jsonParsingError);
    }
    else
    {
        // Exception thrown here.
        for(NSDictionary *item in jsonArray)
        {
            NSLog(@"%@", item);
            NSDictionary * userDict = [item objectForKey:@"user"];
            NSLog(@"%@", [userDict objectForKey:@"username"]);
            NSString *parsedUser = [NSString stringWithFormat:@"%@ \u2022 Username: %@ \n\u2022 Role: %@ \n\u2022 Email: %@ \n\n----------\n\n", txtParsed.text, [userDict objectForKey:@"username"], [userDict objectForKey:@"role"], [userDict objectForKey:@"email"]];
            txtParsed.text = parsedUser;
            /*for(NSDictionary *i in item)
            {
                NSLog(@"%@", i);
                //NSString *parsedUser = [NSString stringWithFormat:@"* Username: %@ \n Role: %@ \n Email: %@ \n\n----------\n\n", [i objectForKey:@"username"], [i objectForKey:@"role"], [i objectForKey:@"email"]];
                //txtParsed.text = i;
            }*/
        }
    }
    
    //NSLog(@"json: %@", json);
    NSString *stringRep = [NSString stringWithFormat:@"json: %@",jsonArray];
    jsonTextBox.text = stringRep;
    jsonTextBox.scrollEnabled = YES;
    jsonTextBox.showsVerticalScrollIndicator = YES;
}

@end
