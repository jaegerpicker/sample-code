//
//  caQAppDelegate.h
//  EverDev
//
//  Created by Shawn Campbell on 8/8/12.
//  Copyright (c) 2012 Shawn Campbell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface caQAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
