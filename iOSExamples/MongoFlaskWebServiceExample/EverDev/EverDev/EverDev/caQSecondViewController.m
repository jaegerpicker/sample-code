//
//  caQSecondViewController.m
//  EverDev
//
//  Created by Shawn Campbell on 8/8/12.
//  Copyright (c) 2012 Shawn Campbell. All rights reserved.
//

#import "caQSecondViewController.h"

@interface caQSecondViewController ()

@end

@implementation caQSecondViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

@end
