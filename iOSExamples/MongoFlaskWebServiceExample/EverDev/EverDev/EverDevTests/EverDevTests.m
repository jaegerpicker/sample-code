//
//  EverDevTests.m
//  EverDevTests
//
//  Created by Shawn Campbell on 8/8/12.
//  Copyright (c) 2012 Shawn Campbell. All rights reserved.
//

#import "EverDevTests.h"

@implementation EverDevTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in EverDevTests");
}

@end
