//
//  caQDetailViewController.h
//  MongoExample
//
//  Created by Shawn Campbell on 7/30/12.
//  Copyright (c) 2012 Shawn Campbell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface caQDetailViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
