//
//  caQMasterViewController.h
//  MongoExample
//
//  Created by Shawn Campbell on 7/30/12.
//  Copyright (c) 2012 Shawn Campbell. All rights reserved.
//

#import <UIKit/UIKit.h>

@class caQDetailViewController;

@interface caQMasterViewController : UITableViewController

@property (strong, nonatomic) caQDetailViewController *detailViewController;

@end
