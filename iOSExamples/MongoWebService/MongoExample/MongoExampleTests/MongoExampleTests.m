//
//  MongoExampleTests.m
//  MongoExampleTests
//
//  Created by Shawn Campbell on 7/30/12.
//  Copyright (c) 2012 Shawn Campbell. All rights reserved.
//

#import "MongoExampleTests.h"

@implementation MongoExampleTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in MongoExampleTests");
}

@end
