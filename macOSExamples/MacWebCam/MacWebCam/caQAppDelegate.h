//
//  caQAppDelegate.h
//  WebCamTester
//
//  Created by Shawn Campbell on 6/19/12.
//  Copyright (c) 2012 Napkin Studio. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QTKit/QTKit.h>
#import <Cocoa/Cocoa.h>
//#import "HTTPServer.h"

@interface caQAppDelegate : NSObject <NSApplicationDelegate>
{
    IBOutlet QTCaptureView      *mCaptureView;
    
    QTCaptureSession            *mSession;
    QTCaptureMovieFileOutput    *mMovieFileOutput;
    QTCaptureDeviceInput        *mDeviceVideoInput;
    QTCaptureDeviceInput        *mDeviceAudioInput;
    //HTTPServer                  *mServer;
}


@property (assign) IBOutlet NSWindow *window;

@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;


- (IBAction)saveAction:(id)sender;
- (IBAction)startRecording:(id)sender;
- (IBAction)stopRecording:(id)sender;
- (IBAction)startHTTPServer:(id)sender;
- (IBAction)stopHTTPServer:(id)sender;

@end
