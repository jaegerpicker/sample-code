//
//  main.m
//  MacWebCam
//
//  Created by Shawn Campbell on 7/16/12.
//  Copyright (c) 2012 Napkin Studio. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
