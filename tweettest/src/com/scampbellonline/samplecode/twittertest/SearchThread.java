package com.scampbellonline.samplecode.twittertest;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.*;

public class SearchThread extends Thread {
	public EditText txtSearch;
	public EditText txtResponse;
	public Handler mHandle;
	
	public SearchThread(EditText s, Handler h)
	{
		mHandle = h;
		txtSearch = s;
	}

	public void run()
	{

        runSearch(txtSearch);
	}

	private void runSearch(EditText txtSearch) {
		if(txtSearch.getText().toString() != "")
        {
        	Searcher s = new Searcher();
        	SearchResponse sr = s.getSearchResponse(txtSearch.getText().toString(), 1);
        	for(Tweet t : sr.results)
        	{
        		Message msg = mHandle.obtainMessage();
        		Bundle data = new Bundle();
        		data.putString("separtor", "------------------------------\n");
                data.putString("from_user", "@" + t.from_user + "\n");
                data.putString("tweeted_at", "Tweeted at: " + t.created_at.toString() + "\n");
                data.putString("tweet", t.text);
                msg.setData(data);
                msg.obj = t;
                mHandle.sendMessage(msg);
        	}
        }
	}

}
