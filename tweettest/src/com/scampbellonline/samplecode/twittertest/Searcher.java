package com.scampbellonline.samplecode.twittertest;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;

public class Searcher 
{
	public SearchResponse getSearchResponse(String searchTerm, int page) 
	{
		ArrayList<Tweet> tweets = new ArrayList<Tweet>();
		String responseBody = doRequest(searchTerm, page);
		JSONObject jsonObject = getResponseBody(responseBody);
		JSONArray arr = getResults(jsonObject);
		buildTweets(tweets, arr);
		SearchResponse sr = buildSearchResponse(tweets, jsonObject);
		return sr;
	}

	private SearchResponse buildSearchResponse(ArrayList<Tweet> tweets,	JSONObject jsonObject) 
	{
		SearchResponse sr = null;
		try
		{
			sr = new SearchResponse(
								(float)jsonObject.getDouble("completed_in") //$NON-NLS-1$
								, jsonObject.getLong("max_id") //$NON-NLS-1$
								, jsonObject.getString("max_id_str") //$NON-NLS-1$
								, jsonObject.getString("next_page") //$NON-NLS-1$
								, jsonObject.getString("page") //$NON-NLS-1$
								, jsonObject.getString("query") //$NON-NLS-1$
								, jsonObject.getString("refresh_url") //$NON-NLS-1$
								, tweets
								, jsonObject.getInt("results_per_page") //$NON-NLS-1$
								, jsonObject.getLong("since_id") //$NON-NLS-1$
								, jsonObject.getString("since_id_str") //$NON-NLS-1$
								); 
		}
		catch (JSONException ex)
		{
			Log.v("TEST", "sr Exception: " + ex.getMessage()); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return sr;
	}

	private void buildTweets(ArrayList<Tweet> tweets, JSONArray arr) 
	{
		for(int i = 0; i<= arr.length(); i++) 
		{
			JSONObject t;
			Tweet tweet;
			try 
			{
				t = arr.getJSONObject(i);
				JSONObject metadataJSON = t.getJSONObject("metadata"); //$NON-NLS-1$
				TweetMetadata meta = new TweetMetadata(metadataJSON.getString("result_type")); //$NON-NLS-1$
				Date created = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH).parse(t.getString("created_at"));
				tweet = new Tweet(
						created //Should be a fornatted date
						, t.getString("from_user") //$NON-NLS-1$
						, t.getLong("from_user_id") //$NON-NLS-1$
						, t.getString("from_user_id_str") //$NON-NLS-1$
						, t.getString("from_user_name") //$NON-NLS-1$ compile and install
						, t.getString("geo") //$NON-NLS-1$
						, t.getLong("id") //$NON-NLS-1$
						, t.getString("id_str") //$NON-NLS-1$
						, t.getString("iso_language_code") //$NON-NLS-1$
						, t.getString("text") //$NON-NLS-1$
						, t.getString("profile_image_url") //$NON-NLS-1$
						, meta
						, t.getString("profile_image_url") //$NON-NLS-1$
						, t.getString("profile_image_url_https") //$NON-NLS-1$
						, t.getString("source") //$NON-NLS-1$
						, t.getString("to_user") //$NON-NLS-1$
						, 0
						, t.getString("to_user_id_str") //$NON-NLS-1$
						, t.getString("to_user_name") //$NON-NLS-1$
						);
				tweets.add(tweet);
			} 
			catch (JSONException e) 
			{
				Log.v("TEST", "tw Exception: " + e.getMessage()); //$NON-NLS-1$ //$NON-NLS-2$
			} catch (ParseException pe) {
				Log.v("TEST", "Parse Exception: " + pe.getMessage());
			}
		    
		}
	}

	private JSONArray getResults(JSONObject jsonObject) 
	{
		JSONArray arr = null;
		    
		try 
		{
			arr = (JSONArray)jsonObject.get("results"); //$NON-NLS-1$
		} 
		catch(Exception ex)
		{
			Log.v("TEST","arr Exception: " + ex.getMessage()); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return arr;
	}

	private JSONObject getResponseBody(String responseBody) 
	{
		JSONObject jsonObject = null;
		    
		try 
		{
			jsonObject = new JSONObject(responseBody);
		}
		catch(Exception ex)
		{
			Log.v("TEST","json Exception: " + ex.getMessage()); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return jsonObject;
	}

	public String doRequest(String searchTerm, int page) 
	{
		String responseBody = null;
		try 
		{
			//URL Twitter search in json format
			String searchUrl = Messages.getString("Searcher.requestURI") + searchTerm + Messages.getString("Searcher.requestURIParams") + page; //$NON-NLS-1$ //$NON-NLS-2$
			HttpClient client = new  DefaultHttpClient();
			HttpGet get = new HttpGet();
			get.setURI(new URI(searchUrl));
		        
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
		
			responseBody = client.execute(get, responseHandler);
		} 
		catch(Exception ex) 
		{
			Log.v("TEST", "res Exception: " + ex.getMessage()); //$NON-NLS-1$ //$NON-NLS-2$
		}
		return responseBody;
	}
}
