package com.scampbellonline.samplecode.twittertest;

import java.util.*;

public class Tweet {
	public Date created_at;
	public String from_user;
	public long from_user_id;
	public String from_user_id_str;
	public String from_username;
	public String geo;
	public long id;
	public String id_str;
	public String iso_language_code;
	public String image_url;
	public TweetMetadata metadata;
	public String profile_image_url;
	public String profile_image_url_https;
	public String source;
	public String text;
	public String to_user;
	public long to_user_id;
	public String to_user_id_str;
	public String to_user_name;
	
	
	
	public Tweet(Date created_at, String from_user, long from_user_id, String from_user_id_str, String from_username,
			String geo, long id, String id_str, String iso_language_code, String text, String image_url, 
			TweetMetadata metadata, String profile_image_url, String profile_image_url_https, String source, String to_user
			, long to_user_id, String to_user_id_str, String to_user_name)
	{
		this.from_username = from_username;
		this.text = text;
		this.image_url = image_url;
		this.created_at = created_at;
		this.from_user = from_user;
		this.from_user_id = from_user_id;
		this.from_user_id_str = from_user_id_str;
		this.geo = geo;
		this.id = id;
		this.id_str = id_str;
		this.iso_language_code = iso_language_code;
		this.metadata = metadata;
		this.profile_image_url = profile_image_url;
		this.profile_image_url_https = profile_image_url_https;
		this.source = source;
		this.to_user = to_user;
		this.to_user_id = to_user_id;
		this.to_user_id_str = to_user_id_str;
		this.to_user_name = to_user_name;
	}

}
