package com.scampbellonline.samplecode.twittertest;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.*;

public class TweetAdapter extends ArrayAdapter<Tweet> 
{
	private Activity act;
	public ArrayList<Tweet> tweets;
	private static LayoutInflater inflator = null;

	public TweetAdapter(Activity a, ArrayList<Tweet> t) 
	{
		super(a, R.id.txtResults);
		act = a;
		this.tweets = t;
		inflator = (LayoutInflater)act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public int getCount()
	{
		return tweets.size();
	}
	
	public Tweet getTweet(int position)
	{
		return tweets.get(position);
	}
	
	public long getPosition(int position)
	{
		return position;
	}
	
	
	public View getView(int p, View convertView, ViewGroup parent)
	{
		View v = convertView;
		ViewHolder holder;
		if(convertView == null)
		{
			v = inflator.inflate(R.layout.list_item, null);
			holder = new ViewHolder();
			holder.img = (ImageView)v.findViewById(R.id.imgAvatar);
			holder.txt = (TextView)v.findViewById(R.id.txtTweet);
			holder.user = (TextView)v.findViewById(R.id.txtUserName);
			v.setTag(holder);
		}
		else
		{
			holder = (ViewHolder)v.getTag();
		}
		
		return v;
	}
	
	public static class ViewHolder
	{
		public ImageView img;
		public TextView txt;
		public TextView user;
	}

}
