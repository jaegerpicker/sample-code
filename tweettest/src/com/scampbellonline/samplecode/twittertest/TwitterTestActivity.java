package com.scampbellonline.samplecode.twittertest;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class TwitterTestActivity extends Activity 
{
    
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);    
        
        Button btn1 = (Button)findViewById(R.id.btnSearch);
        btn1.setOnClickListener(btnListener);
        //setListAdapter(new ArrayAdapter<String>(this, R.layout.list_item, COUNTRIES));
    }
    
    
    private OnClickListener btnListener = new OnClickListener()
    {
        public void onClick(View v)
        {                        
            Toast.makeText(getBaseContext(), "Searching...", Toast.LENGTH_LONG).show();
    		EditText txtSearch = (EditText)findViewById(R.id.txtSearch);
            Thread t = new SearchThread(txtSearch, handler);
            t.start();
        }
    }; 
    
    final Handler handler = new Handler() 
    {
        public void handleMessage(Message msg) 
        {
            ListView txtResponse = (ListView)findViewById(R.id.txtResults);
            Bundle data = msg.getData();
            setListAdaptor(new ArrayAdapter<Tweet>(txtResponse, R.layout.list_item, msg.obj));
            //txtResponse.append(data.getString("separtor"));
            //txtResponse.append(data.getString("from_user"));
            //txtResponse.append(data.getString("tweeted_at"));
            //txtResponse.append(data.getString("tweet"));
        }
    };
}