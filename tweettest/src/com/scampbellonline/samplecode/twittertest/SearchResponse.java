package com.scampbellonline.samplecode.twittertest;

import java.util.ArrayList;

public class SearchResponse {
	public float completed_in;
	public long max_id;
	public String max_id_str;
	public String next_page;
	public String page;
	public String query;
	public String refresh_url;
	public ArrayList<Tweet> results;
	public int results_per_page;
	public long since_id;
	public String since_id_str;
	
	public SearchResponse(float completed_in, long max_id, String max_id_str, String next_page, String page, 
							String query, String refresh_url, ArrayList<Tweet> results, int results_per_page, long since_id, String since_id_str)
	{
		this.completed_in = completed_in;
		this.max_id = max_id;
		this.max_id_str = max_id_str;
		this.next_page = next_page;
		this.page = page;
		this.query = query;
		this.refresh_url = refresh_url;
		this.results = results;
		this.results_per_page = results_per_page;
	}

}
