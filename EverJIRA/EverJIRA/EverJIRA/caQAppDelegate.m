//
//  caQAppDelegate.m
//  EverJIRA
//
//  Created by Shawn Campbell on 6/28/12.
//  Copyright (c) 2012 Napkin Studio. All rights reserved.
//

#import "caQAppDelegate.h"
#import "EvernoteSDK.h"


@implementation caQAppDelegate

@synthesize window = _window;

@property (nonatomic, strong) NSArray *notebooks;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    [self authenticate];
    
    
}

- (IBAction)authenticate:(id)sender 
{
    EvernoteSession *session = [EvernoteSession sharedSession];
    [session authenticateWithViewController:self completionHandler:^(NSError *error) {
        if (error || !session.isAuthenticated) {
            UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Error" 
                                                             message:@"Could not authenticate" 
                                                            delegate:nil 
                                                   cancelButtonTitle:@"OK" 
                                                   otherButtonTitles:nil] autorelease];
            [alert show];
        } else {
            NSLog(@"authenticated! noteStoreUrl:%@ webApiUrlPrefix:%@", session.noteStoreUrl, session.webApiUrlPrefix);
            [self updateButtonsForAuthentication];
        } 
    }];
}

- (IBAction)listNotebooks:(id)sender {
    EvernoteNoteStore *noteStore = [EvernoteNoteStore noteStore];
    [noteStore listNotebooksWithSuccess:^(NSArray *notebooks) {
        self.notebooks = notebooks;
        [self loadNoteBooks];
    }
                                failure:^(NSError *error) {
                                    NSLog(@"error %@", error);                                            
                                }];
}

-(void)loadNoteBooks
{
    for (id object in self.notebooks) 
    {
        [self.listNotebooksButton setStringValue:object.name];
        
    }
}

- (IBAction)logout:(id)sender {
    [[EvernoteSession sharedSession] logout];
    [self updateButtonsForAuthentication];
    self.notebooks = nil;
    //[self.tableView reloadData];
}

- (void)updateButtonsForAuthentication 
{    
    EvernoteSession *session = [EvernoteSession sharedSession];
    
    if (session.isAuthenticated) {
        self.authenticateButton.enabled = NO;
        self.authenticateButton.alpha = 0.5;
        self.listNotebooksButton.enabled = YES;
        self.listNotebooksButton.alpha = 1.0;
        self.logoutButton.enabled = YES;
        self.logoutButton.alpha = 1.0; 
    } else {
        self.authenticateButton.enabled = YES;
        self.authenticateButton.alpha = 1.0;
        self.listNotebooksButton.enabled = NO;
        self.listNotebooksButton.alpha = 0.5;
        self.logoutButton.enabled = NO;
        self.logoutButton.alpha = 0.5;
    }
}

- (void)dealloc {
    //[_tableView release];
    [_notebooks release];
    [_authenticateButton release];
    [_listNotebooksButton release];
    [_logoutButton release];
    [super dealloc];
}


@end
