//
//  main.m
//  EverJIRA
//
//  Created by Shawn Campbell on 6/28/12.
//  Copyright (c) 2012 Napkin Studio. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
