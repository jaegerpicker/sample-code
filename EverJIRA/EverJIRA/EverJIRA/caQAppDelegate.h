//
//  caQAppDelegate.h
//  EverJIRA
//
//  Created by Shawn Campbell on 6/28/12.
//  Copyright (c) 2012 Napkin Studio. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface caQAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

- (void)loadEverNoteNotebooks;

@end
