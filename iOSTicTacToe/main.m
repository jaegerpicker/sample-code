//
//  main.m
//  iOSTicTacToe
//
//  Created by Shawn Campbell on 11/5/12.
//  Copyright (c) 2012 Shawn Campbell. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "caAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([caAppDelegate class]));
    }
}
