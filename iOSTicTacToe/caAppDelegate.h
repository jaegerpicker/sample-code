//
//  caAppDelegate.h
//  iOSTicTacToe
//
//  Created by Shawn Campbell on 11/5/12.
//  Copyright (c) 2012 Shawn Campbell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface caAppDelegate : UIResponder <UIApplicationDelegate> 
@property (strong, nonatomic) UIWindow *window;

@end
