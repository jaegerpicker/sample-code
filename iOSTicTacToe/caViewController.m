//
//  caViewController.m
//  iOSTicTacToe
//
//  Created by Shawn Campbell on 11/5/12.
//  Copyright (c) 2012 Shawn Campbell. All rights reserved.
//

#import "caViewController.h"

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

// Uniform index.
enum
{
    UNIFORM_MODELVIEWPROJECTION_MATRIX,
    UNIFORM_NORMAL_MATRIX,
    NUM_UNIFORMS
};
GLint uniforms[NUM_UNIFORMS];

// Attribute index.
enum
{
    ATTRIB_VERTEX,
    ATTRIB_NORMAL,
    NUM_ATTRIBUTES
};

GLfloat gCubeVertexData[216] = 
{
    // Data layout for each line below is:
    // positionX, positionY, positionZ,     normalX, normalY, normalZ,
    0.5f, -0.5f, -0.5f,        1.0f, 0.0f, 0.0f,
    0.5f, 0.5f, -0.5f,         1.0f, 0.0f, 0.0f,
    0.5f, -0.5f, 0.5f,         1.0f, 0.0f, 0.0f,
    0.5f, -0.5f, 0.5f,         1.0f, 0.0f, 0.0f,
    0.5f, 0.5f, -0.5f,          1.0f, 0.0f, 0.0f,
    0.5f, 0.5f, 0.5f,         1.0f, 0.0f, 0.0f,
    
    0.5f, 0.5f, -0.5f,         0.0f, 1.0f, 0.0f,
    -0.5f, 0.5f, -0.5f,        0.0f, 1.0f, 0.0f,
    0.5f, 0.5f, 0.5f,          0.0f, 1.0f, 0.0f,
    0.5f, 0.5f, 0.5f,          0.0f, 1.0f, 0.0f,
    -0.5f, 0.5f, -0.5f,        0.0f, 1.0f, 0.0f,
    -0.5f, 0.5f, 0.5f,         0.0f, 1.0f, 0.0f,
    
    -0.5f, 0.5f, -0.5f,        -1.0f, 0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,       -1.0f, 0.0f, 0.0f,
    -0.5f, 0.5f, 0.5f,         -1.0f, 0.0f, 0.0f,
    -0.5f, 0.5f, 0.5f,         -1.0f, 0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,       -1.0f, 0.0f, 0.0f,
    -0.5f, -0.5f, 0.5f,        -1.0f, 0.0f, 0.0f,
    
    -0.5f, -0.5f, -0.5f,       0.0f, -1.0f, 0.0f,
    0.5f, -0.5f, -0.5f,        0.0f, -1.0f, 0.0f,
    -0.5f, -0.5f, 0.5f,        0.0f, -1.0f, 0.0f,
    -0.5f, -0.5f, 0.5f,        0.0f, -1.0f, 0.0f,
    0.5f, -0.5f, -0.5f,        0.0f, -1.0f, 0.0f,
    0.5f, -0.5f, 0.5f,         0.0f, -1.0f, 0.0f,
    
    0.5f, 0.5f, 0.5f,          0.0f, 0.0f, 1.0f,
    -0.5f, 0.5f, 0.5f,         0.0f, 0.0f, 1.0f,
    0.5f, -0.5f, 0.5f,         0.0f, 0.0f, 1.0f,
    0.5f, -0.5f, 0.5f,         0.0f, 0.0f, 1.0f,
    -0.5f, 0.5f, 0.5f,         0.0f, 0.0f, 1.0f,
    -0.5f, -0.5f, 0.5f,        0.0f, 0.0f, 1.0f,
    
    0.5f, -0.5f, -0.5f,        0.0f, 0.0f, -1.0f,
    -0.5f, -0.5f, -0.5f,       0.0f, 0.0f, -1.0f,
    0.5f, 0.5f, -0.5f,         0.0f, 0.0f, -1.0f,
    0.5f, 0.5f, -0.5f,         0.0f, 0.0f, -1.0f,
    -0.5f, -0.5f, -0.5f,       0.0f, 0.0f, -1.0f,
    -0.5f, 0.5f, -0.5f,        0.0f, 0.0f, -1.0f
};

@interface caViewController () {
    GLuint _program;
    
    GLKMatrix4 _modelViewProjectionMatrix;
    GLKMatrix3 _normalMatrix;
    float _rotation;
    
    GLuint _vertexArray;
    GLuint _vertexBuffer;
    int player;
}
@property (strong, nonatomic) EAGLContext *context;
@property (strong, nonatomic) GLKBaseEffect *effect;

- (void)setupGL;
- (void)tearDownGL;

- (BOOL)loadShaders;
- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file;
- (BOOL)linkProgram:(GLuint)prog;
- (BOOL)validateProgram:(GLuint)prog;
@end

@implementation caViewController

@synthesize fifthButton;
@synthesize firstButton;
@synthesize secondButton;
@synthesize thridButton;
@synthesize fourthButton;
@synthesize sixthButton;
@synthesize seventhButton;
@synthesize eigthButton;
@synthesize ninthButton;
@synthesize GameButton;
@synthesize pTurn;

- (void)viewDidLoad
{
    [super viewDidLoad];
    player = 1;
    [[firstButton layer] setBorderWidth:2.0f];
    [[firstButton layer] setBorderColor:[UIColor yellowColor].CGColor];
    [[secondButton layer] setBorderWidth:2.0f];
    [[secondButton layer] setBorderColor:[UIColor yellowColor].CGColor];
    [[thridButton layer] setBorderWidth:2.0f];
    [[thridButton layer] setBorderColor:[UIColor yellowColor].CGColor];
    [[fourthButton layer] setBorderWidth:2.0f];
    [[fourthButton layer] setBorderColor:[UIColor yellowColor].CGColor];
    [[fifthButton layer] setBorderWidth:2.0f];
    [[fifthButton layer] setBorderColor:[UIColor yellowColor].CGColor];
    [[sixthButton layer] setBorderWidth:2.0f];
    [[sixthButton layer] setBorderColor:[UIColor yellowColor].CGColor];
    [[seventhButton layer] setBorderWidth:2.0f];
    [[seventhButton layer] setBorderColor:[UIColor yellowColor].CGColor];
    [[eigthButton layer] setBorderWidth:2.0f];
    [[eigthButton layer] setBorderColor:[UIColor yellowColor].CGColor];
    [[ninthButton layer] setBorderWidth:2.0f];
    [[ninthButton layer] setBorderColor:[UIColor yellowColor].CGColor];
    [firstButton addTarget:self action:@selector(firstButtonClick:) forControlEvents:(UIControlEvents)UIControlEventTouchDown];
    [secondButton addTarget:self action:@selector(secondButtonClick:) forControlEvents:(UIControlEvents)UIControlEventTouchDown];
    [thridButton addTarget:self action:@selector(thridButtonClick:) forControlEvents:(UIControlEvents)UIControlEventTouchDown];
    [fourthButton addTarget:self action:@selector(fourthButtonClick:) forControlEvents:(UIControlEvents)UIControlEventTouchDown];
    [fifthButton addTarget:self action:@selector(fifthButtonClick:) forControlEvents:(UIControlEvents)UIControlEventTouchDown];
    [sixthButton addTarget:self action:@selector(sixthButtonClick:) forControlEvents:(UIControlEvents)UIControlEventTouchDown];
    [seventhButton addTarget:self action:@selector(seventhButtonClick:) forControlEvents:(UIControlEvents)UIControlEventTouchDown];
    [eigthButton addTarget:self action:@selector(eigthButtonClick:) forControlEvents:(UIControlEvents)UIControlEventTouchDown];
    [ninthButton addTarget:self action:@selector(ninthButtonClick:) forControlEvents:(UIControlEvents)UIControlEventTouchDown];
    [GameButton addTarget:self action:@selector(GameButtonClick:) forControlEvents:(UIControlEvents)UIControlEventTouchDown];
    //self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];

    //if (!self.context) {
    //    NSLog(@"Failed to create ES context");
    //}
    
    //GLKView *view = (GLKView *)self.view;
    //view.context = self.context;
    //view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    
    //[self setupGL];
}

- (void)firstButtonClick:(id)sender {
    /*if (player == 1) {
            firstButton.backgroundColor = [UIColor greenColor];
    } else {
            firstButton.backgroundColor = [UIColor redColor];
    }
    setPlayersTurn();*/
    [self makeMove:firstButton];
}

-(void)setPlayersTurn{
    if(player < 2)
    {
        if(player == 1)
        {
            player = 2;
        }
        else
        {
            player = 1;
        }
    }
    else {
        player = 1;
    }
    NSString* myNewString = [NSString stringWithFormat:@"%d", player];
    NSLog(myNewString);
    pTurn.text = myNewString;
}

- (void)secondButtonClick:(id)sender {
    /*if (player == 1) {
        secondButton.backgroundColor = [UIColor greenColor];
    } else {
        secondButton.backgroundColor = [UIColor redColor];
    }
    setPlayersTurn();*/
    [self makeMove:secondButton];

}
- (void)thridButtonClick:(id)sender {
    /*if (player == 1) {
        thridButton.backgroundColor = [UIColor greenColor];
    } else {
        thridButton.backgroundColor = [UIColor redColor];
    }
    setPlayersTurn();*/
    [self makeMove:thridButton];
}
- (void)fourthButtonClick:(id)sender {
    /*if (player == 1) {
        fourthButton.backgroundColor = [UIColor greenColor];
    } else {
        fourthButton.backgroundColor = [UIColor redColor];
    }
    setPlayersTurn();*/
    [self makeMove:fourthButton];
}
- (void)fifthButtonClick:(id)sender {
    /*if (player == 1) {
        fifthButton.backgroundColor = [UIColor greenColor];
    } else {
        fifthButton.backgroundColor = [UIColor redColor];
    }
    setPlayersTurn();*/
    [self makeMove:fifthButton];
}
- (void)sixthButtonClick:(id)sender {
    /*if (player == 1) {
        sixthButton.backgroundColor = [UIColor greenColor];
    } else {
        sixthButton.backgroundColor = [UIColor redColor];
    }
    setPlayersTurn();*/
    [self makeMove:sixthButton ];
}
- (void)seventhButtonClick:(id)sender {
    /*if (player == 1) {
        seventhButton.backgroundColor = [UIColor greenColor];
    } else {
        seventhButton.backgroundColor = [UIColor redColor];
    }
    setPlayersTurn();*/
    [self makeMove:seventhButton];
}
- (void)eigthButtonClick:(id)sender {
    /*if (player == 1) {
        eigthButton.backgroundColor = [UIColor greenColor];
    } else {
        eigthButton.backgroundColor = [UIColor redColor];
    }
    setPlayersTurn();*/
    [self makeMove:eigthButton];
}
- (void)ninthButtonClick:(id)sender {
    /*if (player == 1 && (ninthButton.tintColor != [UIColor greenColor] || ninthButton.tintColor != [UIColor redColor])) {
        ninthButton.backgroundColor = [UIColor greenColor];
    } else if (ninthButton.tintColor != [UIColor greenColor] || ninthButton.tintColor != [UIColor redColor]) {
        ninthButton.backgroundColor = [UIColor redColor];
    }
    setPlayersTurn();*/
    [self makeMove:ninthButton];
}
- (void)GameButtonClick:(id)sender {
    firstButton.backgroundColor = [UIColor whiteColor];
    secondButton.backgroundColor = [UIColor blackColor];
    thridButton.backgroundColor = [UIColor whiteColor];
    fourthButton.backgroundColor = [UIColor blackColor];
    fifthButton.backgroundColor = [UIColor whiteColor];
    sixthButton.backgroundColor = [UIColor blackColor];
    seventhButton.backgroundColor = [UIColor whiteColor];
    eigthButton.backgroundColor = [UIColor blackColor];
    ninthButton.backgroundColor = [UIColor whiteColor];
    [self setPlayersTurn];//setPlayersTurn();
}
- (void)makeMove:(UIButton*)btn {
    if(btn.backgroundColor != [UIColor greenColor] && btn.backgroundColor != [UIColor redColor]) {
        if(player == 1) {
            btn.backgroundColor = [UIColor greenColor];
            [self setPlayersTurn ];
        }
        else {
            btn.backgroundColor = [UIColor redColor];
            [self setPlayersTurn];
        }
    }
}

- (void)dealloc
{    
    [self tearDownGL];
    
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

    if ([self isViewLoaded] && ([[self view] window] == nil)) {
        self.view = nil;
        
        [self tearDownGL];
        
        if ([EAGLContext currentContext] == self.context) {
            [EAGLContext setCurrentContext:nil];
        }
        self.context = nil;
    }

    // Dispose of any resources that can be recreated.
}

- (void)setupGL
{
    [EAGLContext setCurrentContext:self.context];
    
    [self loadShaders];
    
    self.effect = [[GLKBaseEffect alloc] init];
    self.effect.light0.enabled = GL_TRUE;
    self.effect.light0.diffuseColor = GLKVector4Make(1.0f, 0.4f, 0.4f, 1.0f);
    
    glEnable(GL_DEPTH_TEST);
    
    glGenVertexArraysOES(1, &_vertexArray);
    glBindVertexArrayOES(_vertexArray);
    
    glGenBuffers(1, &_vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(gCubeVertexData), gCubeVertexData, GL_STATIC_DRAW);
    
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, 24, BUFFER_OFFSET(0));
    glEnableVertexAttribArray(GLKVertexAttribNormal);
    glVertexAttribPointer(GLKVertexAttribNormal, 3, GL_FLOAT, GL_FALSE, 24, BUFFER_OFFSET(12));
    
    glBindVertexArrayOES(0);
}

- (void)tearDownGL
{
    [EAGLContext setCurrentContext:self.context];
    
    glDeleteBuffers(1, &_vertexBuffer);
    glDeleteVertexArraysOES(1, &_vertexArray);
    
    self.effect = nil;
    
    if (_program) {
        glDeleteProgram(_program);
        _program = 0;
    }
}

#pragma mark - GLKView and GLKViewController delegate methods

- (void)update
{
    float aspect = fabsf(self.view.bounds.size.width / self.view.bounds.size.height);
    GLKMatrix4 projectionMatrix = GLKMatrix4MakePerspective(GLKMathDegreesToRadians(65.0f), aspect, 0.1f, 100.0f);
    
    self.effect.transform.projectionMatrix = projectionMatrix;
    
    GLKMatrix4 baseModelViewMatrix = GLKMatrix4MakeTranslation(0.0f, 0.0f, -4.0f);
    baseModelViewMatrix = GLKMatrix4Rotate(baseModelViewMatrix, _rotation, 0.0f, 1.0f, 0.0f);
    
    // Compute the model view matrix for the object rendered with GLKit
    GLKMatrix4 modelViewMatrix = GLKMatrix4MakeTranslation(0.0f, 0.0f, -1.5f);
    modelViewMatrix = GLKMatrix4Rotate(modelViewMatrix, _rotation, 1.0f, 1.0f, 1.0f);
    modelViewMatrix = GLKMatrix4Multiply(baseModelViewMatrix, modelViewMatrix);
    
    self.effect.transform.modelviewMatrix = modelViewMatrix;
    
    // Compute the model view matrix for the object rendered with ES2
    modelViewMatrix = GLKMatrix4MakeTranslation(0.0f, 0.0f, 1.5f);
    modelViewMatrix = GLKMatrix4Rotate(modelViewMatrix, _rotation, 1.0f, 1.0f, 1.0f);
    modelViewMatrix = GLKMatrix4Multiply(baseModelViewMatrix, modelViewMatrix);
    
    _normalMatrix = GLKMatrix3InvertAndTranspose(GLKMatrix4GetMatrix3(modelViewMatrix), NULL);
    
    _modelViewProjectionMatrix = GLKMatrix4Multiply(projectionMatrix, modelViewMatrix);
    
    _rotation += self.timeSinceLastUpdate * 0.5f;
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    glClearColor(0.65f, 0.65f, 0.65f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glBindVertexArrayOES(_vertexArray);
    
    // Render the object with GLKit
    [self.effect prepareToDraw];
    
    glDrawArrays(GL_TRIANGLES, 0, 36);
    
    // Render the object again with ES2
    glUseProgram(_program);
    
    glUniformMatrix4fv(uniforms[UNIFORM_MODELVIEWPROJECTION_MATRIX], 1, 0, _modelViewProjectionMatrix.m);
    glUniformMatrix3fv(uniforms[UNIFORM_NORMAL_MATRIX], 1, 0, _normalMatrix.m);
    
    glDrawArrays(GL_TRIANGLES, 0, 36);
}

#pragma mark -  OpenGL ES 2 shader compilation

- (BOOL)loadShaders
{
    GLuint vertShader, fragShader;
    NSString *vertShaderPathname, *fragShaderPathname;
    
    // Create shader program.
    _program = glCreateProgram();
    
    // Create and compile vertex shader.
    vertShaderPathname = [[NSBundle mainBundle] pathForResource:@"Shader" ofType:@"vsh"];
    if (![self compileShader:&vertShader type:GL_VERTEX_SHADER file:vertShaderPathname]) {
        NSLog(@"Failed to compile vertex shader");
        return NO;
    }
    
    // Create and compile fragment shader.
    fragShaderPathname = [[NSBundle mainBundle] pathForResource:@"Shader" ofType:@"fsh"];
    if (![self compileShader:&fragShader type:GL_FRAGMENT_SHADER file:fragShaderPathname]) {
        NSLog(@"Failed to compile fragment shader");
        return NO;
    }
    
    // Attach vertex shader to program.
    glAttachShader(_program, vertShader);
    
    // Attach fragment shader to program.
    glAttachShader(_program, fragShader);
    
    // Bind attribute locations.
    // This needs to be done prior to linking.
    glBindAttribLocation(_program, GLKVertexAttribPosition, "position");
    glBindAttribLocation(_program, GLKVertexAttribNormal, "normal");
    
    // Link program.
    if (![self linkProgram:_program]) {
        NSLog(@"Failed to link program: %d", _program);
        
        if (vertShader) {
            glDeleteShader(vertShader);
            vertShader = 0;
        }
        if (fragShader) {
            glDeleteShader(fragShader);
            fragShader = 0;
        }
        if (_program) {
            glDeleteProgram(_program);
            _program = 0;
        }
        
        return NO;
    }
    
    // Get uniform locations.
    uniforms[UNIFORM_MODELVIEWPROJECTION_MATRIX] = glGetUniformLocation(_program, "modelViewProjectionMatrix");
    uniforms[UNIFORM_NORMAL_MATRIX] = glGetUniformLocation(_program, "normalMatrix");
    
    // Release vertex and fragment shaders.
    if (vertShader) {
        glDetachShader(_program, vertShader);
        glDeleteShader(vertShader);
    }
    if (fragShader) {
        glDetachShader(_program, fragShader);
        glDeleteShader(fragShader);
    }
    
    return YES;
}

- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file
{
    GLint status;
    const GLchar *source;
    
    source = (GLchar *)[[NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:nil] UTF8String];
    if (!source) {
        NSLog(@"Failed to load vertex shader");
        return NO;
    }
    
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);
    
#if defined(DEBUG)
    GLint logLength;
    glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(*shader, logLength, &logLength, log);
        NSLog(@"Shader compile log:\n%s", log);
        free(log);
    }
#endif
    
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    if (status == 0) {
        glDeleteShader(*shader);
        return NO;
    }
    
    return YES;
}

- (BOOL)linkProgram:(GLuint)prog
{
    GLint status;
    glLinkProgram(prog);
    
#if defined(DEBUG)
    GLint logLength;
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program link log:\n%s", log);
        free(log);
    }
#endif
    
    glGetProgramiv(prog, GL_LINK_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    
    return YES;
}

- (BOOL)validateProgram:(GLuint)prog
{
    GLint logLength, status;
    
    glValidateProgram(prog);
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program validate log:\n%s", log);
        free(log);
    }
    
    glGetProgramiv(prog, GL_VALIDATE_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    
    return YES;
}

@end
