//
//  Shader.fsh
//  iOSTicTacToe
//
//  Created by Shawn Campbell on 11/5/12.
//  Copyright (c) 2012 Shawn Campbell. All rights reserved.
//

varying lowp vec4 colorVarying;

void main()
{
    gl_FragColor = colorVarying;
}
