//
//  caViewController.h
//  iOSTicTacToe
//
//  Created by Shawn Campbell on 11/5/12.
//  Copyright (c) 2012 Shawn Campbell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import <QuartzCore/QuartzCore.h>

@interface caViewController : GLKViewController{
    IBOutlet UIButton *firstButton;
    IBOutlet UIButton *secondButton;
    IBOutlet UIButton *thridButton;
    IBOutlet UIButton *fourthButton;
    IBOutlet UIButton *fifthButton;
    IBOutlet UIButton *sixthButton;
    IBOutlet UIButton *seventhButton;
    IBOutlet UIButton *eigthButton;
    IBOutlet UIButton *ninthButton;
    IBOutlet UIButton *GameButton;
    IBOutlet UILabel *pTurn;
}
@property (nonatomic, retain) UIButton *firstButton;
@property (nonatomic, retain) UIButton *secondButton;
@property (nonatomic, retain) UIButton *thridButton;
@property (nonatomic, retain) UIButton *fourthButton;
@property (nonatomic, retain) UIButton *fifthButton;
@property (nonatomic, retain) UIButton *sixthButton;
@property (nonatomic, retain) UIButton *seventhButton;
@property (nonatomic, retain) UIButton *eigthButton;
@property (nonatomic, retain) UIButton *ninthButton;
@property (nonatomic, retain) UIButton *GameButton;
@property (nonatomic, retain) UILabel *pTurn;

@end
