var imgLoadedSinceLastClick = 0;

function itsOnRandom() {
	$.ajax({
		url: '/ranImg',
		success: function(data) {
			//alert(data.imgName);
			$('#imgList tr.alert').removeClass('alert');
			var fullSizes = "<table>"
			//alert(data.sizes);
			var j = 0;
			$.each(data.sizes, function(i)
			{
				j++;
				if(data.sizes[i] == 'true')
				{
					fullSizes += '<tr><td><a href="https://d2vn94glaxzkz3.cloudfront.net/wp-content/uploads/wallpapers/'  + data.imgName + '-' + i + '.' + data.imgType + '" target=_blank>' + i + '</a>';
					if(j % 2 == 0)
					{
						fullSizes += '</tr></td>';
					}
					else
					{
						fullSizes += '<br>';
					}
				}
			});
			fullSizes += '</table>';
			//alert(fullSizes);
			$('#topRow').after('<tr class="alert"><td><img src="https://d2vn94glaxzkz3.cloudfront.net/wp-content/uploads/2012/02/' + data.imgName + '-' + data.cropImg + '.' + data.imgType + '"></img></td><td>' + fullSizes + '</td></tr>');
			twitterStyleAlert(data.imgName + ' loaded!');
			//alert('Load was performed.');
  		},
		error: function(x, h, q) {
			alert('x: ' + x + ' h: ' + h + ' q: ' + q);
		}
	});
}

function twitterStyleAlert(messToDisplay) {
	imgLoadedSinceLastClick++;
	if($('#alertmsg').length == 0)
	{
		var $alertdiv = $('<div id = "alertmsg"/>');
	}
	else
	{
		var $alertdiv = $('#alertmsg');
	}
    $alertdiv.html(messToDisplay + ' New images: [' + String(imgLoadedSinceLastClick) + ']');
	document.title = "[" + String(imgLoadedSinceLastClick) + "] Guild Wars 2 Wallpaper Viewer";
    $alertdiv.bind('click', function() {
    	$(this).slideUp(200);
		imgLoadedSinceLastClick = 0;
		document.title = "Guild Wars 2 Wallpaper Viewer";
		window.scrollTo(0,0);
    });
    $(document.body).append($alertdiv);
    $("#alertmsg").slideDown("slow"); 
	setTimeout(function() { $alertdiv.slideUp(200) }, 10000);
}

setInterval(function() {
    //call $.ajax here
	itsOnRandom();
}, 15000);

$(document).ready(function() {
	itsOnRandom();
});