from flask import Flask, jsonify, render_template, request
import json
from bson import json_util
from flask import make_response
from random import randrange
import pymongo


app = Flask(__name__)
    
@app.route('/ranImg')
def ranImg():
    mongo = pymongo.Connection('127.0.0.1')
    db = mongo['local']
    coll = db['wallpapers']
    img = coll.find().skip(randrange(1, coll.find().count())).limit(1)[0]
    print img
    res = make_response(json.dumps(img,default=json_util.default))
    res.headers['Content-Type'] = 'application/json'
    return res
    
@app.route('/')
def index():
    return render_template('notify.html')    
    
if __name__ == '__main__':
    app.run(debug=True)