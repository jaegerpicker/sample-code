import os
import notify
import unittest
import tempfile



class NotifyTest(unittest.TestCase):

    def setUp(self):
        self.db_fd, notify.app.config['DATABASE'] = tempfile.mkstemp()
        notify.app.config['TESTING'] = True
        self.app = notify.app.test_client()
        #notify.init_db()
        
    def ranImg(self):
        return self.app.get('/ranImg', follow_redirects=True)
        
    def test_ranImg(self):
        rv = self.ranImg()
        #print rv.data
        assert rv.data.count('cropImg') > 0
    
    def tearDown(self):
            os.close(self.db_fd)
            os.unlink(notify.app.config['DATABASE'])
        
if __name__ == '__main__':
    unittest.main()