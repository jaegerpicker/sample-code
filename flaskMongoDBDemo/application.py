from flask import Flask
from flask import render_template
from flask import json
from flask import jsonify
import pymongo
import json
from pymongo import Connection
from bson import json_util
from flask.ext.pymongo import PyMongo
from flask import make_response

app = Flask(__name__)
mongo = PyMongo(app)

@app.route('/')
def home_page():
    conn = Connection()
    online_users = conn.local.users.find()
    app.logger.debug(online_users)
    app.logger.debug('test')
    return render_template('hello.html', online_users=online_users)
    
@app.route('/users.json/<role>')
def users_json(role):
    conn = Connection() 
    json_users = []
    online_users = conn.local.users.find({'role': role})
    for ju in online_users:
        json_users.append({"user" : ju})
    app.logger.debug(online_users)
    app.logger.debug(json_users)
    response = make_response(json.dumps(json_users, default=json_util.default))
    response.headers['Content-Type'] = "application/json"
    return response
    
@app.route('/user.json/<uname>')
def user_json(uname):
    conn = Connection()
    json_user = []
    online_user = conn.local.users.find({'username': uname})
    app.logger.debug(online_user)
    for ju in online_user:
        json_user.append({"user": ju})
    response = make_response(json.dumps(json_user, default=json_util.default))
    response.headers['Content-Type'] = 'application/json'
    return response
    
#@app.route('/adduser.json/<uname>')
    
if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0')