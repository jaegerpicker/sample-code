from django.http import HttpResponse
from djagno.views.generic import View

def index(request):
    return HttpResponse("Hello blogging world")
    
def post(request):
    return HttpResponse("Posts will go here!")

def comment(request):
    return HttpResponse("Comments!")