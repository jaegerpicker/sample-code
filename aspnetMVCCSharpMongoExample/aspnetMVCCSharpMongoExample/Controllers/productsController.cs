﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;
using aspnetMVCCSharpMongoExample.Models;
namespace aspnetMVCCSharpMongoExample.Controllers
{
    public class productsController : Controller
    {
        //
        // GET: /products/

        public ActionResult Index()
        {
            var connectionString = "mongodb://192.168.0.106/?safe=true";
            var server = MongoServer.Create(connectionString);
            var database = server.GetDatabase("local");
            var collection = database.GetCollection<productSheet>("sheets");
            var ps = new productSheet { ProductName = "Tom", ProductType = "iceCream" };
            collection.Insert(ps);
            var id = ps.Id;
            ViewBag.Message = ps.ProductName + " ice cream has an id of: " + id.ToString();
            return View();
        }

    }
}
