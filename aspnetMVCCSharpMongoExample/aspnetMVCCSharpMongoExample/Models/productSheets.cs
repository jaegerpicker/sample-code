﻿using System;

namespace aspnetMVCCSharpMongoExample.Models
{
    public class productSheet
    {
        private string _id;

        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }
        private string _productName;

        public string ProductName
        {
            get { return _productName; }
            set { _productName = value; }
        }
        private string _productType;

        public string ProductType
        {
            get { return _productType; }
            set { _productType = value; }
        }
    }
}